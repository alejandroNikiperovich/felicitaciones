<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $locale = $request->server("HTTP_ACCEPT_LANGUAGE");
        $locale = substr($locale, 0, 2);

        switch ($locale) {
            case "en":
                App::setlocale("en");
                break;
            case "es":
                App::setlocale("es");
                break;
            default:
                App::setlocale("en");
        }

        return $next($request);
    }
}
