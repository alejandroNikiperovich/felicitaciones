<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\DB;

class PDFController extends Controller
{
    public function generatePDF() {
        $dateMayor = date("Y-m-d", strtotime("-18 year"));
        
        $arrayClientes = DB::table("clientes")
            ->select("nombre", "correo", "fecha_nacimiento")
            ->where("fecha_nacimiento", "<=", $dateMayor)
            ->get();

        $data = [
            "title" => "Clientes mayores de edad",
            "date" => date("m/D/Y"),
            "clientes" => $arrayClientes
        ];

        $pdf = PDF::loadView("myPDF", $data);
        return $pdf->download("mayoresEdad.pdf");
    }
}
