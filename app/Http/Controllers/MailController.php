<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MailController extends Controller
{
    public function html_email() {
        $data = array("name" => "Virat Gandhi");

        Mail::send("mail", $data, function($message) {
            $message->to("alejandronikiperovich@gmail.com", "Tutorials Point")->subject
                ("Laravel HTML Testing Mail");
            $message->from("xyz@gmail.com", "Virat Gandhi");
        });

        echo "HTML Email Sent. Check your inbox";
    }

    public function felicita() {
        $arrayClientes = DB::table("clientes")
            ->whereMonth("fecha_nacimiento", date("n"))
            ->whereDay("fecha_nacimiento", date("j"))->get();

        foreach ($arrayClientes as $cliente) {
            $datosCliente = array("name" => $cliente->nombre);
            Mail::send("mail", $datosCliente, function($message) use($cliente) {
                $message->to($cliente->correo, $cliente->nombre)->subject(env("APP_NAME"));
                $message->from(config("mail.from.address"), config("mail.from.name"));
                $message->attach($cliente->imagen);
            });
        }

        echo "Email Sent with congratulations! Check your inbox.";
    }
}
