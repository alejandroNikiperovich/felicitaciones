<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\ClientsExport;
use App\Imports\ClientsImport;
use Maatwebsite\Excel\Facades\Excel;

class CSVController extends Controller
{
    public function importExportView() {
        return view("import");
    }

    public function export() {
        return Excel::download(new ClientsExport, "clients.xlsx");
    }

    public function import() {
        Excel::import(new ClientsImport, request()->file("file"));
        return back();
    }
}
