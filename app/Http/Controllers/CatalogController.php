<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cliente;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ClienteFormRequest;
use Illuminate\Support\Facades\DB;

class CatalogController extends Controller
{
    public function getIndex(Request $request) {
        $texto = trim($request->get("texto"));

        $arrayClientes = DB::table("clientes")
            ->select()
            ->where("nombre", "LIKE", "%" . $texto . "%")
            ->paginate(8);

        return view("index", compact("arrayClientes", "texto"));
    }

    public function getShow($id) {
        $cliente = Cliente::findOrFail($id);
        return view("show", ["cliente" => $cliente]);
    }

    public function getCreate() {
        return view("create");
    }

    public function postCreate(Request $request) {
        $validator = Validator::make($request->all(), [
            "title" => "required|string|max:32",
            "imagen" => "mimes:png,jpg|max:2048",
            "fecha_nacimiento" => "required|date",
            "email" => "required|email|max:255"
        ]);

        if ($validator->fails()) {
            return redirect("catalog/create")
                        ->withErrors($validator)
                        ->withInput();
        }

        $request->file("imagen")->store("public");

        $cliente = new Cliente;
        $cliente->nombre = $request->input("title");
        $cliente->imagen = asset("storage/" . $request->file("imagen")->hashName());
        $cliente->fecha_nacimiento = $request->input("fecha_nacimiento");
        $cliente->correo = $request->input("email");
        $cliente->save();

        $request->session()->flash("correcto", "Se ha creado el registro");
        return redirect("catalog");
    }

    public function getEdit($id) {
        $cliente = Cliente::findOrFail($id);
        return view("edit", ["cliente" => $cliente]);
    }

    public function putEdit(ClienteFormRequest $request, $id) {
        $request->file("imagen")->store("public");

        $cliente = Cliente::findOrFail($id);
        $cliente->nombre = $request->input("title");
        $cliente->imagen = asset("storage/" . $request->file("imagen")->hashName());
        $cliente->fecha_nacimiento = $request->input("fecha_nacimiento");
        $cliente->correo = $request->input("email");
        $cliente->save();

        $request->session()->flash("correcto", "Se ha editado el cliente");
        return redirect("catalog");
    }

    public function putDelete(Request $request, $id) {
        $cliente = Cliente::findOrFail($id);
        $cliente->delete();

        $request->session()->flash("correcto", "Se ha borrado el cliente");
        return redirect("catalog");
    }
}
