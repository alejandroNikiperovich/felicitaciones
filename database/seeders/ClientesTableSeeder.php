<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Cliente;

class ClientesTableSeeder extends Seeder
{
    private $arrayClientes = array(
        array(
            "id" => 0,
            "nombre" => "Neo",
            "imagen" => "https://i.pinimg.com/736x/49/4e/bd/494ebdb49a253cb56075079ab6466f08.jpg",
            "fecha_nacimiento" => "1972-01-06",
            "correo" => "neo@neo.es"
        ),

        array(
            "id" => 1,
            "nombre" => "Ruby",
            "imagen" => "https://i.pinimg.com/originals/bd/6e/36/bd6e368591cdf60c0af6e5b211e96713.jpg",
            "fecha_nacimiento" => "1997-03-05",
            "correo" => "ruby@ruby.es"
        )
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("clientes")->delete();

        Cliente::factory()->count(50)->create();

        foreach($this->arrayClientes as $cliente) {
            $c = new Cliente;
            $c->nombre = $cliente['nombre'];
            $c->imagen = $cliente['imagen'];
            $c->fecha_nacimiento = $cliente['fecha_nacimiento'];
            $c->correo = $cliente['correo'];
            $c->save();
        }

        DB::table("clientes")->insert([
            "nombre" => "Jose",
            "fecha_nacimiento" => "1970-10-30",
            "correo" => "jose@gmail.com"
        ]);

        Cliente::create(["nombre" => "Ruiz", "fecha_nacimiento" => "1997-10-30", "correo" => "alejandronikiperovich@googlemail.com"]);
        Cliente::create(["nombre" => "Nikiperovich", "fecha_nacimiento" => "1997-10-30", "correo" => "alejandronikiperovich@gemail.com"]);
    }
}
