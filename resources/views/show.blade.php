@extends("layouts.master")

@section("content")

    <div class="row">
        <div class="col-sm-4">
            <img src="{{$cliente->imagen}}" />
        </div>

        </div class="col-sm-8">
            <h4>{{$cliente->nombre}}</h4>

            <p>
                <b>Correo-e: </b>
                {{$cliente->correo}}
            </p>

            <p>
                <b>Fecha de nacimiento: </b>
                {{$cliente->fecha_nacimiento}}
            </p>

            <a href="{{url('/catalog/edit/' . $cliente->id)}}" class="btn btn-warning" role="button">Editar</a>

            <form method="post" action="{{url('/catalog/delete') . '/' . $cliente->id}}" style="display:inline">
                @method("DELETE")

                @csrf

                <button type="submit" class="btn btn-danger" role="button">
                    Borrar
                </button>
            </form>

            <a href="/catalog" class="btn btn-info" role="button">< Volver</a>
        </div>
    </div>

@stop