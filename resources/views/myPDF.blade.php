<!DOCTYPE html>
<html>
    <head>
        <title>PDF</title>
    </head>

    <body>
        <h1>{{ $title }}</h1>
        <p>{{ $date }}</p>

        <table>
            <tr>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Edad</th>
            </tr>

            @foreach($clientes as $key => $cliente)
            <tr>
                <td>{{ $cliente->nombre }}</td>
                <td>{{ $cliente->correo }}</td>
                <td>{{ date("Y") - date("Y", strtotime($cliente->fecha_nacimiento)) }}</td>
            </tr>
            @endforeach
        </table>
    </body>
</html>