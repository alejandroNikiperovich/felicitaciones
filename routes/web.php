<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('auth/login', function () {
    return view("login");
});

Route::get('auth/logout', function () {
    return 'Logout usuario';
})->middleware("auth");

Route::get("sendhtmlemail", [MailController::class, "html_email"]);
Route::get("birthday", [MailController::class, "felicita"]);
Route::get("generate-pdf", [PDFController::class, "generatePDF"]);

Route::get("importExportView", [CSVController::class, "importExportView"]);
Route::get("export", [CSVController::class, "export"])->name("export");
Route::post("import", [CSVController::class, "import"])->name("import");

Auth::routes(["verify" => "true"]);

Route::group(["middleware" => "verified"], function() {
    Route::get('/', [HomeController::class, "getHome"])->middleware("language");

    Route::get('catalog', [CatalogController::class, "getIndex"])->middleware("auth");

    Route::get('catalog/show/{id}', [CatalogController::class, "getShow"])->middleware("auth");
    
    Route::get('catalog/create', [CatalogController::class, "getCreate"])->middleware("auth");

    Route::post("catalog/create", [CatalogController::class, "postCreate"])->middleware("auth");
    
    Route::get('catalog/edit/{id}', [CatalogController::class, "getEdit"])->middleware("auth");

    Route::put("catalog/edit/{id}", [CatalogController::class, "putEdit"])->middleware("auth");
    
    Route::delete('catalog/delete/{id}', [CatalogController::class, "putDelete"])->middleware("auth");
});

Route::get('/home', [HomeController::class, 'index'])->name('home');
